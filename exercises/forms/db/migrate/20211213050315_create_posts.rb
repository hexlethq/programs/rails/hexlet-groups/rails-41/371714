class CreatePosts < ActiveRecord::Migration[6.1]
  def change
    create_table :posts do |t|
      t.string :title, default: '', null: false
      t.text :body, default: '', null: false
      t.string :summary, default: '', null: false
      t.boolean :published, default: false, null: false

      t.timestamps
    end
  end
end
