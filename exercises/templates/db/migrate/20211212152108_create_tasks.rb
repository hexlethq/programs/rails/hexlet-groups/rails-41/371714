class CreateTasks < ActiveRecord::Migration[6.1]
  def change
    create_table :tasks do |t|
      t.string :name, default: '', null: false
      t.text :description, default: '', null: false
      t.string :status, default: 'new', null: false
      t.string :creator, default: '', null: false
      t.string :performer, default: '', null: false
      t.boolean :completed, default: false, null: false

      t.timestamps
    end
  end
end
