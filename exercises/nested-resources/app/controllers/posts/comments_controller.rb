# frozen_string_literal: true

module Posts
  class CommentsController < ApplicationController
    before_action :find_post
    before_action :find_comment, only: [:edit, :update, :destroy]

    def create
      @comment = @post.comments.build(comment_params)
      if @comment.save
        redirect_to @post, notice: 'Comment was created'
      else
        render 'posts/show', notice: 'Comment\'s body can\'t be blank!'
      end
    end

    def edit; end

    def update
      if @comment.update(comment_params)
        redirect_to @post, notice: 'Comment was updated'
      else
        render :edit, notice: 'Comment\'s body can\'t be blank!'
      end
    end

    def destroy
      @comment.destroy
      redirect_to @post, notice: 'Comment was destroyed'
    end

    private

    def find_post
      @post = Post.find(params[:post_id])
    end

    def find_comment
      @comment = Comment.find(params[:id])
    end

    def comment_params
      params.require(:comment).permit(:body)
    end
  end
end
