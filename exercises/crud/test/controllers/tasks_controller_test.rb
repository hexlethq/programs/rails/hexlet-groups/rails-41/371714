# frozen_string_literal: true

require 'test_helper'

class TasksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @task = tasks(:one)
  end

  test 'open all tasks page' do
    get tasks_path

    assert_response :success
    assert_select 'a', 'Tasks'
    assert_select 'a', 'New task'
    assert_select 'a', 'Task 1'
    assert_select 'a', 'Task 2'
  end

  test 'open task page' do
    get task_path(@task)

    assert_response :success
    assert_select 'h2', 'Task 1'
    assert_select 'p', 'Task description'
  end

  test 'open new task page' do
    get new_task_path

    assert_response :success
  end

  test 'create new taks' do
    post tasks_path, params: { task: { name: 'Task 3', status: 're-open', creator: 'PM', performer: 'DEV' } }

    task = Task.find_by! name: 'Task 3'

    assert_redirected_to task_url(task)
  end

  test 'failing create new task' do
    post tasks_path, params: { task: { name: '', status: '', creator: '' } }

    assert_response :success
    assert_select 'li', "Name can't be blank"
    assert_select 'li', "Status can't be blank"
    assert_select 'li', "Creator can't be blank"
  end

  test 'update task' do
    patch task_path(@task), params: { task: { status: 'close', completed: true } }

    task = Task.find_by! name: 'Task 1'

    assert task.completed
    assert_equal task.status, 'close'
    assert_redirected_to task_url(task)
  end

  test 'destroy task' do
    delete task_path(@task)

    tasks = Task.all

    assert_equal tasks.count, 1
    assert_redirected_to tasks_url
  end
end
